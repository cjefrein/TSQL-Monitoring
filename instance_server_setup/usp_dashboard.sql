USE msdb
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'monitoring')
EXEC('CREATE SCHEMA monitoring')
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'usp_dashboard')
	EXEC ('CREATE PROC monitoring.usp_dashboard AS SELECT ''stub version, to be replaced''')
GO


ALTER PROCEDURE monitoring.usp_dashboard
	
AS

SET NOCOUNT ON
DECLARE @BCHR bigint;
DECLARE @batchrequestsparseconde bigint; 
DECLARE @BRPS bigint;
DECLARE @compilationsparseconde bigint; 
DECLARE @CPS bigint;
DECLARE @recompilationsparseconde bigint;
DECLARE @RCPS bigint;
DECLARE @lockwaitsparseconde bigint; 
DECLARE @LWPS bigint;
DECLARE @pagesplitsparseconde bigint;
DECLARE @PSPS bigint;
DECLARE @checkpointpagesparseconde bigint;
DECLARE @CPPS bigint;
DECLARE @DLPS bigint;


DECLARE @PLE bigint;
DECLARE @UC bigint; 
DECLARE @PB bigint;
DECLARE @DATESTAT datetime;
DECLARE @datemesure datetime; 


DECLARE @statavant table ([object_name] VARCHAR (128) 
			, [counter_name] VARCHAR (128) 
			, [instance_name] VARCHAR (128) 
			, [cntr_value] bigint 
			, [cntr_type] int); 

DECLARE @statapres table ([object_name] VARCHAR (128) 
			, [counter_name] VARCHAR (128) 
			, [instance_name] VARCHAR (128) 
			, [cntr_value] bigint 
			, [cntr_type] int);
			 
SET @datemesure = GETDATE(); 

INSERT into @statavant ([object_name] 
			, [counter_name] 
			, [instance_name] 
			, [cntr_value] 
			, [cntr_type]) 
SELECT [object_name] 
	, [counter_name] 
	, [instance_name] 
	, [cntr_value] 
	, [cntr_type] 
FROM sys.dm_os_performance_counters 
WHERE (counter_name = 'Buffer cache hit ratio' 
			AND object_name like '%Buffer Manager%') 
	OR (counter_name = 'Buffer cache hit ratio base' 
			AND object_name like '%Buffer Banager%') 
	OR (counter_name = 'Page life expectancy ' 
			AND object_name like '%Buffer Manager%') 
	OR (counter_name = 'Batch Requests/sec' 
			AND object_name like '%SQL Statistics%') 
	OR (counter_name = 'SQL Compilations/sec' 
			AND object_name like '%SQL Statistics%') 
	OR (counter_name = 'SQL Re-Compilations/sec' 
			AND object_name like '%SQL Statistics%') 
	OR (counter_name = 'User Connections' 
			AND object_name like '%General Statistics%') 
	OR (counter_name = 'Lock Waits/sec' 
			AND instance_name = '_Total' 
			AND object_name like '%Locks%') 
	OR (counter_name = 'Page Splits/sec' 
			AND object_name like '%Access Methods%') 
	OR (counter_name = 'Processes blocked' 
			AND object_name like '%General Statistics%') 
	OR (counter_name = 'Checkpoint pages/sec' 
			AND object_name like '%Buffer Manager%') 
	OR (counter_name = 'Number of Deadlocks/sec' 
			AND instance_name = '_Total' 
			AND object_name like '%Locks%') 
; 

SELECT top 1 @batchrequestsparseconde = cntr_value 
FROM @statavant 
WHERE counter_name = 'Batch Requests/sec' 
	AND object_name like '%SQL Statistics%'; 

SELECT top 1 @compilationsparseconde = cntr_value 
FROM @statavant 
WHERE counter_name = 'SQL Compilations/sec' 
	AND object_name like '%SQL Statistics%'; 

SELECT top 1 @recompilationsparseconde = cntr_value 
FROM @statavant 
WHERE counter_name = 'SQL Re-Compilations/sec' 
	AND object_name like '%SQL Statistics%'; 

SELECT top 1 @lockwaitsparseconde = cntr_value 
FROM @statavant 
WHERE counter_name = 'Lock Waits/sec' 
	AND instance_name = '_Total' 
	AND object_name like '%Locks%'; 

SELECT top 1 @pagesplitsparseconde = cntr_value 
FROM @statavant 
WHERE counter_name = 'Page Splits/sec' 
	AND object_name like '%Access Methods%'; 

SELECT top 1 @checkpointpagesparseconde = cntr_value 
FROM @statavant 
WHERE counter_name = 'Checkpoint pages/sec' 
	AND object_name like '%Buffer Manager%'; 


	
waitfor delay '00:00:01'; 

INSERT into @statapres ([object_name] 
			, [counter_name] 
			, [instance_name] 
			, [cntr_value] 
			, [cntr_type]) 

SELECT [object_name] 
	, [counter_name] 
	, [instance_name] 
	, [cntr_value] 
	, [cntr_type] 
FROM sys.dm_os_performance_counters 
WHERE (counter_name = 'Buffer cache hit ratio' 
			AND object_name like '%Buffer Manager%') 
	OR (counter_name = 'Buffer cache hit ratio base' 
			AND object_name like '%Buffer Manager%') 
	OR (counter_name = 'Page life expectancy ' 
			AND object_name like '%Buffer Manager%') 
	OR (counter_name = 'Batch Requests/sec' 
			AND object_name like '%SQL Statistics%') 
	OR (counter_name = 'SQL Compilations/sec' 
			AND object_name like '%SQL Statistics%') 
	OR (counter_name = 'SQL Re-Compilations/sec' 
			AND object_name like '%SQL Statistics%') 
	OR (counter_name = 'User Connections' 
			AND object_name like '%General Statistics%') 
	OR (counter_name = 'Lock Waits/sec' 
			AND instance_name = '_Total' 
			AND object_name like '%Locks%') 
	OR (counter_name = 'Page Splits/sec' 
			AND object_name like '%Access Methods%') 
	OR (counter_name = 'Processes blocked' 
			AND object_name like '%General Statistics%') 
	OR (counter_name = 'Checkpoint pages/sec' 
			AND object_name like '%Buffer Manager%') 
	OR (counter_name = 'Number of Deadlocks/sec' 
			AND instance_name = '_Total' 
			AND object_name like '%Locks%') 
; 

SELECT @BCHR = floor(round(a.cntr_value * 1.0 / b.cntr_value * 100, 0)) 
	, @PLE =  c.cntr_value
	, @BRPS = d.[batchrequestspersecond] 
	, @CPS = e.[compilationspersecond] 
	, @RCPS = f.[recompilationspersecond] 
	, @UC = g.cntr_value
	, @LWPS = h.lockwaitspersecond 
	, @PSPS = i.pagesplitspersecond 
	, @PB = j.cntr_value
	, @CPPS = k.checkpointpagespersecond 
	, @DLPS = l.cntr_value
	, @DATESTAT = GETDATE () 
FROM (
		SELECT * 
		FROM @statapres 
		WHERE counter_name = 'Buffer cache hit ratio' 
			AND object_name like '%Buffer Manager%') a 
	CROSS JOIN (
		SELECT * 
		FROM @statapres 
		WHERE counter_name = 'Buffer cache hit ratio base' 
			AND object_name like '%Buffer Manager%') b 
	CROSS JOIN (
		SELECT * 
		FROM @statapres 
		WHERE counter_name = 'Page life expectancy ' 
			AND object_name like '%Buffer Manager%') c 
	CROSS JOIN (
		SELECT (cntr_value - @batchrequestsparseconde) / (case when DATEDIFF (ss , @datemesure , GETDATE ()) = 0 then 1 
				ELSE DATEDIFF (ss , @datemesure , GETDATE ()) end) as [batchrequestspersecond] 
		FROM @statapres 
		WHERE counter_name = 'Batch Requests/sec' 
			AND object_name like '%SQL Statistics%') d 
	CROSS JOIN (
		SELECT (cntr_value - @compilationsparseconde) / (case when DATEDIFF (ss , @datemesure , GETDATE ()) = 0 then 1 
				ELSE DATEDIFF (ss , @datemesure , GETDATE ()) end) as [compilationspersecond] 
		FROM @statapres 
		WHERE counter_name = 'SQL Compilations/sec' 
			AND object_name like '%SQL Statistics%') e 
	CROSS JOIN (
		SELECT (cntr_value - @recompilationsparseconde) / (case when DATEDIFF (ss , @datemesure , GETDATE ()) = 0 then 1 
				ELSE DATEDIFF (ss , @datemesure , GETDATE ()) end) as [recompilationspersecond] 
		FROM @statapres 
		WHERE counter_name = 'SQL Re-Compilations/sec' 
			AND object_name like '%SQL Statistics%') f 
	CROSS JOIN (
		SELECT * 
		FROM @statapres 
		WHERE counter_name = 'User Connections' 
			AND object_name like '%General Statistics%') g 
	CROSS JOIN (
		SELECT (cntr_value - @lockwaitsparseconde) / (case when DATEDIFF (ss , @datemesure , GETDATE ()) = 0 then 1 
				ELSE DATEDIFF (ss , @datemesure , GETDATE ()) end) as [lockwaitspersecond] 
		FROM @statapres 
		WHERE counter_name = 'Lock Waits/sec' 
			AND instance_name = '_Total' 
			AND object_name like '%Locks%') h 
	CROSS JOIN (
		SELECT (cntr_value - @pagesplitsparseconde) / (case when DATEDIFF (ss , @datemesure , GETDATE ()) = 0 then 1 
				ELSE DATEDIFF (ss , @datemesure , GETDATE ()) end) as [pagesplitspersecond] 
		FROM @statapres 
		WHERE counter_name = 'Page Splits/sec' 
			AND object_name like '%Access Methods%') i 
	CROSS JOIN (
		SELECT * 
		FROM @statapres 
		WHERE counter_name = 'Processes blocked' 
			AND object_name like '%General Statistics%') j 
	CROSS JOIN (
		SELECT (cntr_value - @checkpointpagesparseconde) / (case when DATEDIFF (ss , @datemesure , GETDATE ()) = 0 then 1 
				ELSE DATEDIFF (ss , @datemesure , GETDATE ()) end) as [checkpointpagespersecond] 
		FROM @statapres 
		WHERE counter_name = 'Checkpoint pages/sec' 
			AND object_name like '%Buffer Manager%') k 
	CROSS JOIN (
		SELECT *
		FROM @statapres 
		WHERE counter_name = 'Number of Deadlocks/sec' 
			AND instance_name = '_Total' 
			AND object_name like '%Locks%') l


DECLARE @metrics_table AS TABLE(metric_id int, metric_name varchar(50))
INSERT INTO @metrics_table
SELECT metric_id, metric_name FROM [MONITORING_INSTANCE].[MONITORING].[monitoring].[metrics]


SET NOCOUNT ON
SELECT metric_id, @BRPS FROM @metrics_table mm WHERE metric_name = 'batch_requests_per_second'
UNION
SELECT metric_id, @CPS FROM @metrics_table mm WHERE metric_name = 'compilations_per_second'
UNION
SELECT metric_id, @RCPS FROM @metrics_table mm WHERE metric_name = 'recompilations_per_second'
UNION
SELECT metric_id, @LWPS FROM @metrics_table mm WHERE metric_name = 'lock_waits_per_second'
UNION
SELECT metric_id, @PSPS FROM @metrics_table mm WHERE metric_name = 'page_splits_per_second'
UNION
SELECT metric_id, @CPPS FROM @metrics_table mm WHERE metric_name = 'checkpoint_pages_per_second'
UNION
SELECT metric_id, @DLPS FROM @metrics_table mm WHERE metric_name = 'Deadlocks'
UNION
SELECT metric_id, @BCHR FROM @metrics_table mm WHERE metric_name = 'buffer_cache_hit_ratio'
UNION
SELECT metric_id, @PLE FROM @metrics_table mm WHERE metric_name = 'ple'
UNION
SELECT metric_id, @UC FROM @metrics_table mm WHERE metric_name = 'user_count'
UNION
SELECT metric_id, @PB FROM @metrics_table mm WHERE metric_name = 'blocked_processes'

SET NOCOUNT OFF




