USE [master]
GO

/****** Object:  LinkedServer [MONITORING_INSTANCE]    Script Date: 18/01/2018 10:37:28 ******/
EXEC master.dbo.sp_addlinkedserver @server = N'MONITORING_INSTANCE', @srvproduct=N'SQL Server'
 /* For security reasons the linked server remote logins password is changed with ######## */
EXEC master.dbo.sp_addlinkedsrvlogin @rmtsrvname=N'MONITORING_INSTANCE',@useself=N'False',@locallogin=NULL,@rmtuser=N'monitoring_labo',@rmtpassword='z3bXsy3HiY5osR5p8f86'

GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'collation compatible', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'data access', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'dist', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'pub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'rpc', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'rpc out', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'sub', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'connect timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'collation name', @optvalue=null
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'lazy schema validation', @optvalue=N'false'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'query timeout', @optvalue=N'0'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'use remote collation', @optvalue=N'true'
GO

EXEC master.dbo.sp_serveroption @server=N'MONITORING_INSTANCE', @optname=N'remote proc transaction promotion', @optvalue=N'true'
GO



