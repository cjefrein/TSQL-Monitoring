USE MONITORING
GO

SELECT
mi.instance_name,
mm.metric_name, 
datetimestamp,
ms.value
from [MONITORING].[monitoring].[monitor_statistics] ms
INNER JOIN [MONITORING].[monitoring].[metrics] mm ON mm.metric_id = ms.metric_id
INNER JOIN [MONITORING].[monitoring].[monitored_instances] mi ON mi.instance_id = ms.instance_id
where 1=1
