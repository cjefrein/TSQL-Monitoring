CREATE DATABASE [MONITORING]
GO

/* SETUP */


USE [MONITORING]
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'monitoring')
EXEC('CREATE SCHEMA monitoring')
GO

IF OBJECT_ID('monitoring.monitor_graphs_metrics', 'U') IS NOT NULL
BEGIN
DROP TABLE [monitoring].[monitor_graphs_metrics]
END


IF OBJECT_ID('monitoring.monitor_statistics', 'U') IS NOT NULL
BEGIN
DROP TABLE [monitoring].[monitor_statistics]
END

IF OBJECT_ID('monitoring.monitored_instances', 'U') IS NOT NULL
BEGIN
DROP TABLE [monitoring].[monitored_instances]
END

IF OBJECT_ID('monitoring.metrics', 'U') IS NOT NULL
BEGIN
DROP TABLE [monitoring].[metrics]
END

IF OBJECT_ID('monitoring.stored_procedures', 'U') IS NOT NULL
BEGIN 
DROP TABLE [monitoring].[stored_procedures]
END

IF OBJECT_ID('monitoring.monitor_graphs', 'U') IS NOT NULL
BEGIN
DROP TABLE [monitoring].[monitor_graphs]
END

-- monitored instances
CREATE TABLE [monitoring].[monitored_instances](
	[instance_id] int identity(1,1),
	[instance_name] varchar(50),
	CONSTRAINT [PK_monitored_instaces] PRIMARY KEY CLUSTERED(
		[instance_id]
	)
);

-- INSERT monitored instances
INSERT INTO [monitoring].[monitored_instances] ([instance_name]) SELECT srvname FROM sys.sysservers WHERE srvname <> @@SERVERNAME
--select * from [monitoring].monitored_instances


-- list of stored procedures
CREATE TABLE [monitoring].[stored_procedures](
	[usp_id]  int identity(1,1),
	[usp_name] varchar(50),
	CONSTRAINT [PK_stored_procedures] PRIMARY KEY CLUSTERED(
		[usp_id]
	)
);

INSERT INTO [monitoring].[stored_procedures] VALUES
('monitoring.usp_cpu_sql_system')
,('monitoring.usp_dashboard')
,('monitoring.usp_longest_running_transaction')

INSERT INTO [monitoring].[stored_procedures] VALUES
('monitoring.usp_tempdb_latency')

-- metrics
CREATE TABLE [monitoring].[metrics](
	[metric_id]  int identity(1,1),
	[metric_name] varchar(50),
	[stored_procedure_id] int,
	CONSTRAINT [FK_usp_id] FOREIGN KEY([stored_procedure_id]) REFERENCES[monitoring].[stored_procedures]([usp_id]),
	CONSTRAINT [PK_metrics] PRIMARY KEY CLUSTERED(
		[metric_id]
	)
);

CREATE NONCLUSTERED INDEX [idx_usp_id] ON [monitoring].[metrics]([stored_procedure_id]);


-- every metric name must match a value in the corresponding stored procedure. 
INSERT INTO [monitoring].[metrics] ([metric_name], [stored_procedure_id]) VALUES
('sql_cpu',1)
,('system_cpu',1)
,('batch_requests_per_second',2)
,('compilations_per_second',2)
,('recompilations_per_second',2)
,('lock_waits_per_second',2)
,('page_splits_per_second',2)
,('checkpoint_pages_per_second',2)
,('Deadlocks',2)
,('buffer_cache_hit_ratio',2)
,('ple',2)
,('user_count',2)
,('blocked_processes',2)
,('Longest_Running_Transaction',3)

INSERT INTO [monitoring].[metrics] ([metric_name], [stored_procedure_id]) VALUES
('tempdb_avg_read_latency',4)
,('tempdb_avg_write_latency',4)


select * from [monitoring].[metrics]

-- table where statistics are stored
CREATE TABLE [monitoring].[monitor_statistics](
	[id_statistic] int identity(1,1),
	[metric_id] int,
	[instance_id] int,
	[value] bigint,
	[datetimestamp] datetime2  DEFAULT GETDATE(),
	CONSTRAINT [FK_instance_id] FOREIGN KEY([instance_id]) REFERENCES[monitoring].[monitored_instances]([instance_id]),
	CONSTRAINT [FK_metric_id] FOREIGN KEY([metric_id]) REFERENCES [monitoring].[metrics]([metric_id])
)



CREATE UNIQUE CLUSTERED INDEX [idx_monitor_statistics] ON [monitoring].[monitor_statistics]
(
	[id_statistic] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO

-- drop index [idx_datetimestamp] ON [monitoring].[monitor_statistics]
-- CREATE NONCLUSTERED INDEX [idx_datetimestamp] ON [monitoring].[monitor_statistics]([instance_id], [value], [datetimestamp]) --INCLUDE (, )
CREATE NONCLUSTERED INDEX [idx_instance_id] ON [monitoring].[monitor_statistics]([instance_id]);
CREATE NONCLUSTERED INDEX [idx_metric_id] ON [monitoring].[monitor_statistics]([metric_id]);


/* GRAPHICS */

/* table with graphs */
CREATE TABLE [monitoring].[monitor_graphs](
[id_graph] int IDENTITY(1,1)
,[graph_name] varchar(50)
	CONSTRAINT [PK_graphs] PRIMARY KEY CLUSTERED(
		[id_graph]
	)
)

INSERT INTO [monitoring].[monitor_graphs] ([graph_name]) VALUES 
('CPU')
,('Batch_Request_Per_Second')
,('Compilations_Recompilations')
,('Lock_Waits')
,('Page_Splits')
,('Checkpoints')
,('Deadlocks')
,('Buffer_Cache_Hit_Ration')
,('PLE')
,('User_Count')
,('Blocked_Processes')
,('Longest_Running_Transaction')
,('Tempdb_Latency')


/* table with graphs_metrics associations (lookup table) */
CREATE TABLE [monitoring].[monitor_graphs_metrics](
[id_graphs_metrics] int IDENTITY(1,1)
,[id_graph] int
,[id_metric] int
	CONSTRAINT [PK_graph_metrics] PRIMARY KEY CLUSTERED(
		[id_graphs_metrics]
	),
	CONSTRAINT [FK_id_metric] FOREIGN KEY([id_metric]) REFERENCES [monitoring].[metrics]([metric_id]),
	CONSTRAINT [FK_id_graph] FOREIGN KEY([id_graph]) REFERENCES [monitoring].[monitor_graphs]([id_graph])

)


CREATE NONCLUSTERED INDEX [idx_id_metric] ON [monitoring].[monitor_graphs_metrics]([id_metric]);
CREATE NONCLUSTERED INDEX [idx_id_graph] ON [monitoring].[monitor_graphs_metrics]([id_graph]);



INSERT INTO [monitoring].[monitor_graphs_metrics] ([id_metric], [id_graph]) VALUES 
(1,1) -- sql_cpu
,(2,1) -- system_cpu
,(3,2) -- batch_requests_per_second
,(4,3) -- compilations_per_second
,(5,3) -- recompilations_per_second
,(6,4) -- lock_waits_per_second
,(7,5) -- page_splits_per_second
,(8,6) -- checkpoint_pages_per_second
,(9,7) -- Deadlocks
,(10,8) -- Buffer Cache Hit Ratio
,(11,9) -- ple
,(12,10) -- user_count
,(13,11) -- blocked_processes
,(14,12) -- Longest_Running_Transaction
,(15,13) -- tempdb_avg_read_latency
,(16,13) -- tempdb_avg_write_latency



