USE msdb
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'monitoring')
EXEC('CREATE SCHEMA monitoring')
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'usp_longest_running_transaction')
	EXEC ('CREATE PROC monitoring.usp_longest_running_transaction AS SELECT ''stub version, to be replaced''')
GO


ALTER PROCEDURE monitoring.usp_longest_running_transaction
	
AS



DECLARE @LongestRunningTransaction int;


SELECT
        @LongestRunningTransaction = 
                MAX(DATEDIFF(n, dtat.transaction_begin_time, GETDATE())) 
FROM 
        sys.dm_tran_active_transactions dtat 
        INNER JOIN sys.dm_tran_session_transactions dtst ON dtat.transaction_id = dtst.transaction_id
	INNER JOIN sys.dm_exec_sessions es ON dtst.session_id = es.session_id
	WHERE es.session_id > 50 AND es.login_name <> 'DOLIST-LAN\administrator';



SET NOCOUNT ON
	SELECT metric_id, ISNULL(@LongestRunningTransaction,0) FROM [MONITORING_INSTANCE].[MONITORING].[monitoring].[metrics] mm WHERE metric_name = 'Longest_Running_Transaction'

SET NOCOUNT OFF


