USE msdb
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'monitoring')
EXEC('CREATE SCHEMA monitoring')
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'usp_cpu_sql_system')
	EXEC ('CREATE PROC monitoring.usp_cpu_sql_system AS SELECT ''stub version, to be replaced''')
GO


ALTER PROCEDURE monitoring.usp_cpu_sql_system
	
AS

DECLARE @system_cpu as bigint,
	@sql_cpu as bigint



-- this gives back cpu usage on the instance and hosting machine


DECLARE @ts_now bigint
SELECT @ts_now = cpu_ticks / (cpu_ticks/ms_ticks)  FROM sys.dm_os_sys_info
SELECT @system_cpu = avg(CASE WHEN system_cpu_utilization_post_sp2 IS NOT NULL THEN system_cpu_utilization_post_sp2 ELSE system_cpu_utilization_pre_sp2 END),
  @sql_cpu = avg(CASE WHEN sql_cpu_utilization_post_sp2 IS NOT NULL THEN sql_cpu_utilization_post_sp2 ELSE sql_cpu_utilization_pre_sp2 END)
FROM
(
  SELECT top 30
    record.value('(Record/@id)[1]', 'int') AS record_id,
    DATEADD (ms, -1 * (@ts_now - [timestamp]), GETDATE()) AS EventTime,
    100-record.value('(Record/SchedulerMonitorEvent/SystemHealth/SystemIdle)[1]', 'int') AS system_cpu_utilization_post_sp2,
    record.value('(Record/SchedulerMonitorEvent/SystemHealth/ProcessUtilization)[1]', 'int') AS sql_cpu_utilization_post_sp2 ,
    100-record.value('(Record/SchedluerMonitorEvent/SystemHealth/SystemIdle)[1]', 'int') AS system_cpu_utilization_pre_sp2,
    record.value('(Record/SchedluerMonitorEvent/SystemHealth/ProcessUtilization)[1]', 'int') AS sql_cpu_utilization_pre_sp2
  FROM (
    SELECT timestamp, CONVERT (xml, record) AS record
    FROM sys.dm_os_ring_buffers
    WHERE ring_buffer_type = 'RING_BUFFER_SCHEDULER_MONITOR'
      AND record LIKE '%<SystemHealth>%') AS t
	order by EventTime desc
) AS t

DECLARE @metrics_table AS TABLE(metric_id int, metric_name varchar(50))
INSERT INTO @metrics_table
SELECT metric_id, metric_name FROM [MONITORING_INSTANCE].[MONITORING].[monitoring].[metrics]



SET NOCOUNT ON;

SELECT metric_id, @system_cpu FROM @metrics_table mm WHERE metric_name = 'system_cpu'
UNION
SELECT metric_id, @sql_cpu FROM @metrics_table mm WHERE metric_name = 'sql_cpu'



SET NOCOUNT OFF;


