USE msdb
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'monitoring')
EXEC('CREATE SCHEMA monitoring')
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'usp_tempdb_latency')
	EXEC ('CREATE PROC monitoring.usp_tempdb_latency AS SELECT ''stub version, to be replaced''')
GO


ALTER PROCEDURE monitoring.usp_tempdb_latency
	
AS

DECLARE @AVGTEMPDBWLC as int
DECLARE @AVGTEMPDBRLC as int


SELECT @AVGTEMPDBRLC = SUM(CAST(io_stall_read_ms / num_of_reads AS NUMERIC(10,1))) 
	, @AVGTEMPDBWLC = SUM(CAST(io_stall_write_ms / num_of_writes AS NUMERIC(10,1)))
FROM sys.dm_io_virtual_file_stats(null,null) AS fs 
	INNER JOIN sys.master_files AS mf (NOLOCK) ON fs.database_id = mf.database_id 
		AND fs.[file_id] = mf.[file_id]
WHERE DB_NAME(fs.database_id)='tempdb'
GROUP BY fs.database_id
OPTION (RECOMPILE)

DECLARE @metrics_table AS TABLE(metric_id int, metric_name varchar(50))
INSERT INTO @metrics_table
SELECT metric_id, metric_name FROM [MONITORING_INSTANCE].[MONITORING].[monitoring].[metrics]


SET NOCOUNT ON
	SELECT metric_id, ISNULL(@AVGTEMPDBRLC,0) FROM @metrics_table mm WHERE metric_name = 'tempdb_avg_read_latency'
	UNION
	SELECT metric_id, ISNULL(@AVGTEMPDBWLC,0) FROM @metrics_table mm WHERE metric_name = 'tempdb_avg_write_latency'
SET NOCOUNT OFF
