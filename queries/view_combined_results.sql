-- create table to store intermediate results
IF OBJECT_ID('tempdb..#intermediate_values') IS NOT NULL
BEGIN
    DROP TABLE #intermediate_values
END
CREATE TABLE #intermediate_values(rownumber int, instance_name varchar(50), datetimestamp datetime2, value int default 0)
GO


DECLARE @metric_to_search varchar(50) = 'sql_cpu'
-- rowcount of instances 
DECLARE @rowcount AS INT
-- sqlcommand to execute
DECLARE @SQLCOMMAND AS NVARCHAR(MAX) = ''
-- a table to store all instance names
DECLARE @instancenames AS TABLE(rownum int identity(1,1), instance_name varchar(50))
-- the instance name to collect data from
DECLARE @instance_name varchar(50)
-- total number of instances
DECLARE @totalinstances AS INT
-- first instance name 
DECLARE @first_instance_name AS varchar(50)

-- setup TEMP TABLE ------------------------------------------------------------------------------------------------


-- insert all instance names
INSERT INTO @instancenames (instance_name)
SELECT instance_name FROM [MONITORING].[monitoring].[monitored_instances] (NOLOCK) ORDER BY instance_id DESC

SELECT TOP 1 @first_instance_name=instance_name FROM [MONITORING].[monitoring].[monitored_instances] (NOLOCK) ORDER BY instance_id ASC
-- SELECT @first_instance_name

-- number of rows to process
SELECT @rowcount = COUNT(*) from MONITORING.monitoring.monitored_instances
SELECT @totalinstances = @rowcount

-- while number of rows > 0
WHILE(@rowcount) > 0
BEGIN 

	-- this instance name
	SELECT @instance_name = instance_name FROM @instancenames WHERE rownum = @rowcount

	-- generate sql command
	SET @SQLCOMMAND += '
	SELECT
	ROW_NUMBER() OVER(ORDER BY datetimestamp DESC) as rownumber,
	mi.instance_name,
	datetimestamp,
	ISNULL(ms.value,0) as value
	from [MONITORING].[monitoring].[monitor_statistics] ms (nolock)
	INNER JOIN [MONITORING].[monitoring].[metrics] mm (nolock) ON mm.metric_id = ms.metric_id
	INNER JOIN [MONITORING].[monitoring].[monitored_instances] mi (nolock) ON mi.instance_id = ms.instance_id
	where 1=1
	AND metric_name in ('''+@metric_to_search+''')
	AND mi.instance_name IN ('''+@instance_name+''')
	'

	-- if this is the last instance then don't add union all 
	IF (@rowcount) > 1
		SET @SQLCOMMAND += 'UNION ALL'

	SET @rowcount -= 1

END

--SELECT @SQLCOMMAND

-- insert data into intermediate results table
INSERT INTO #intermediate_values(rownumber, instance_name, datetimestamp , value )
EXEC (@SQLCOMMAND)


-- setup SELECT ----------------------------------------------------------------------------------------------------

-- initialize SQLCOMMAND
SET @SQLCOMMAND = 'SELECT ['+@first_instance_name+'COLUMN].datetimestamp'

-- number of rows to process
SELECT @rowcount = COUNT(*) from MONITORING.monitoring.monitored_instances

-- while number of rows > 0
WHILE(@rowcount) > 0
BEGIN 

	-- this instance name
	SELECT @instance_name = instance_name FROM @instancenames WHERE rownum = @rowcount
	SET @SQLCOMMAND += ', isnull(['++@instance_name++'COLUMN].metric_value,0) AS ['++@instance_name++'COLUMN] '
	SET @rowcount -= 1
END

SET @SQLCOMMAND += ' FROM ('


-- setup FROM AND JOINs------------------------------------------------------------------------------------------

-- number of rows to process
SELECT @rowcount = COUNT(*) from MONITORING.monitoring.monitored_instances

-- while number of rows > 0
WHILE(@rowcount) > 0
BEGIN 

	-- this instance name
	SELECT @instance_name = instance_name FROM @instancenames WHERE rownum = @rowcount

	-- generate sql command
	SET @SQLCOMMAND += '
	SELECT 
	rownumber,
	datetimestamp,
	ISNULL([value],0) as ''metric_value''
	from #intermediate_values
	where 1=1
	AND instance_name = '''+@instance_name+'''
	) as ['++@instance_name++'COLUMN] '



	-- if this row is less than total instances add ON 
	IF (@rowcount) < @totalinstances
		SET @SQLCOMMAND += 'ON ' + '['++@instance_name++'COLUMN].rownumber = ['+@first_instance_name+'COLUMN].rownumber ' + CHAR(13)

	-- if this is the last instance then don't add union all 
	IF (@rowcount) > 1
		BEGIN
			SET @SQLCOMMAND += 'LEFT OUTER JOIN ('
		END 


	SET @rowcount -= 1

END

--SELECT @SQLCOMMAND
SET @SQLCOMMAND +=  CHAR(13) + ' 
WHERE ['+@first_instance_name+'COLUMN].datetimestamp > GETDATE()-1 and ['+@first_instance_name+'COLUMN].datetimestamp <=GETDATE()
ORDER BY ['+@first_instance_name+'COLUMN].datetimestamp'

SELECT @metric_to_search AS METRIC_TYPE

PRINT @SQLCOMMAND
EXEC(@SQLCOMMAND)


