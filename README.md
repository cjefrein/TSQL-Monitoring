TSQL-Monitoring
=========

Tired of monitoring tools that only allow you to view one SQL Server at a time? TSQL-Monitoring is a set of scripts to help you monitor your SQL Servers in combined views. No need to install agents or other software. Once you have chosen and setup your monitoring server, you simply add four stored procedures to each monitored instance. With that accomplished, a SQL Server Agent job on the monitoring server  will execute those stored procedures locally on each monitored instance. Then using a linked server back to the monitoring server, each monitored instance sends back those results to a combined table. 

The monitor_statistics table primary key can be partitioned by instance, thus allowing you to optimize results storage and reduce query execution time when searching specific data on a single or several instances. 

## Getting started

Setup a monitor instance and then deploy the stored procedures onto the instances you wish to monitor. A job will be required to execute SQLCMD commands on each monitored instance

### Requirements

* [SQL Server 2012+] Developer or Enterprise Edition for the Monitor Server
* [SQL Server 2012+] Any version for the monitored instances
* [SQL Server Agent or similar]

### CPU Comparison Example

This chart displays a 24 hour view of CPU activity across all monitored instances


![](./images/cpu_comparison_chart.png)


