USE msdb
GO

IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'monitoring')
EXEC('CREATE SCHEMA monitoring')
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'usp_excute_monitoring')
	EXEC ('CREATE PROC monitoring.usp_excute_monitoring AS SELECT ''stub version, to be replaced''')
GO


ALTER PROCEDURE monitoring.usp_excute_monitoring
AS

DECLARE @SQL AS nvarchar(max)
DECLARE @stored_procedure_id AS int
DECLARE @stored_procedure_name AS varchar(255)


-- open cursor for all stored procedures that will be executed
DECLARE sp_cursor CURSOR FOR 
SELECT [usp_id], [usp_name] FROM [MONITORING_INSTANCE].[MONITORING].[monitoring].[stored_procedures]

OPEN sp_cursor;
FETCH NEXT FROM sp_cursor INTO @stored_procedure_id, @stored_procedure_name;


WHILE @@FETCH_STATUS = 0
   BEGIN
      
		/* drop table ##intermediary_results if it exists */
		IF OBJECT_ID('tempdb..##intermediary_results') IS NOT NULL
			DROP TABLE ##intermediary_results

		/* create ##intermediary_results table */
		CREATE TABLE ##intermediary_results(metric_id int, metric_value bigint)
		INSERT INTO ##intermediary_results
		EXEC @stored_procedure_name

		/* merge results with metrics table and insert into monitor_statistics on SQL-LABO */
		INSERT INTO [MONITORING_INSTANCE].[MONITORING].[monitoring].[monitor_statistics] (instance_id, metric_id, value, [datetimestamp])
		SELECT mi.instance_id, mm.metric_id , irs.metric_value, GETDATE() as datetimestamp
		FROM [MONITORING_INSTANCE].[MONITORING].[monitoring].[metrics] mm
		INNER JOIN ##intermediary_results irs ON irs.metric_id = mm.metric_id
		INNER JOIN [MONITORING_INSTANCE].[MONITORING].[monitoring].[monitored_instances] mi ON mi.instance_name = @@SERVERNAME

	  	  
	  FETCH NEXT FROM sp_cursor INTO @stored_procedure_id, @stored_procedure_name;

   END;


CLOSE sp_cursor;
DEALLOCATE sp_cursor;

/* drop table ##intermediary_results if it exists */
IF OBJECT_ID('tempdb..##intermediary_results') IS NOT NULL
	DROP TABLE ##intermediary_results



GO


